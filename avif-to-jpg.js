// usage: > node avif-to-jpg URL1, URL2, ...

const axios = require('axios');
const sharp = require('sharp');
const fs = require('fs');

// https://cloudinary.images-iherb.com/image/upload/f_auto,q_auto:eco/images/sor/sor08252/y/29.jpg
// https://cloudinary.images-iherb.com/image/upload/f_auto,q_auto:eco/images/sor/sor08252/y/33.jpg

const convert = async () => {
  for (let i = 2; i < process.argv.length; i++) {
    const url = process.argv[i];
    console.log('url: ', url);
    const urlSplit = url.split('/');
    try {
      const input = (await axios({ url, responseType: 'arraybuffer' })).data;
      const dirName = 'output/' + urlSplit[urlSplit.length - 3];
      console.log('folder: ', dirName);

      if (!fs.existsSync('output')) {
        fs.mkdirSync('output');
      }

      if (!fs.existsSync(dirName)) {
        fs.mkdirSync(dirName);
      }

      const fileName = `${urlSplit[urlSplit.length - 2]}_${urlSplit[urlSplit.length - 1]}`;
      await sharp(input).toFormat('jpg').toFile(`${dirName}/${fileName}`);
    } catch (error) {
      console.error('ERROR: ', url);
    }
  }
};

convert();
